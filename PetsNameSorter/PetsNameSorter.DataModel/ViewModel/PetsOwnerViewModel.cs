﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PetsNameSorter.DataModel.ViewModel
{
    public sealed class PetsOwnerViewModel
    {
        public string Gender { get; set; }
        public IList<string> PetNames { get; set; }
    }
}
