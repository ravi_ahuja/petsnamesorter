﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PetsNameSorter.DataModel.Model
{
    public sealed class PetsOwner
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("age")]
        public int Age { get; set; }

        public ICollection<Pet> Pets { get; set; }
    }
}
