using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PetsNameSorter.NetCoreApp;
using Moq;
using System.IO;
using System.Threading.Tasks;

namespace UnitTestProject1
{
    [TestClass]
    public class CatNameSorterTests
    {
        private readonly string GenderMale = "Male";
        private readonly string GenderFemale = "Female";

        public static IConfiguration Configuration { get; set; }
        private readonly string _url;
        private readonly string _invalidUrl;
        private readonly string _invalidjsonUrl;

        public CatNameSorterTests()
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", false, true)
                    .Build();

            _url = Configuration["serviceUrl"];
            _invalidUrl = Configuration["InvalidUrl"];
            _invalidjsonUrl= Configuration["InvalidJsonUrl"];
        }

        [TestMethod]
        public async Task GetCatNames_OwnerSpecific()
        {
            // Moq Object
            var moqValidator = new Mock<IWebServiceUrlValidator>();
            moqValidator.Setup(x => x.IsValid(It.IsAny<string>())).Returns(true);

            // Instantiate
            var sut = new PetsNameSorterEngine(moqValidator.Object);

            // Execute
            var result = await sut.GetSortedCatNames_OwnersGenderwise(_url);

            // Check if NO Error Message
            Assert.AreEqual(string.Empty, result.Item2);

            // Validate
            var rsp = result.Item1;
            Assert.AreEqual(5, rsp.Count);

            // Check Gender of the Owner
            Assert.AreEqual(GenderMale, rsp[0].Gender);
            Assert.AreEqual(GenderFemale, rsp[1].Gender);
            Assert.AreEqual(GenderMale, rsp[2].Gender);
            Assert.AreEqual(GenderFemale, rsp[3].Gender);
            Assert.AreEqual(GenderFemale, rsp[4].Gender);

            // Check the list of Petnames for each Owner in the List.
            // First Owners Pets
            Assert.AreEqual(true, rsp[0].PetNames.Count == 1);

            // Validate the alphabetical sequence of Pets for 1st owner
            var petList = rsp[0].PetNames;
            Assert.AreEqual("Garfield", petList[0]);

            // 2nd Owners Pets
            Assert.AreEqual(true, rsp[1].PetNames.Count == 1);
            // Validate the alphabetical sequence of Pets for 2nd Owner
            petList = rsp[1].PetNames;
            Assert.AreEqual("Garfield", petList[0]);

            // 3rd Owners Pets
            Assert.AreEqual(true, rsp[2].PetNames.Count == 3);
            // Validate the alphabetical sequence of Pets for 3rd Owner
            petList = rsp[2].PetNames;
            Assert.AreEqual("Jim", petList[0]);
            Assert.AreEqual("Max", petList[1]);
            Assert.AreEqual("Tom", petList[2]);

            // 4th Owners Pets
            Assert.AreEqual(true, rsp[3].PetNames.Count == 1);
            // Validate the alphabetical sequence of Pets
            petList = rsp[3].PetNames;
            Assert.AreEqual("Tabby", petList[0]);

            // 5th Owner
            Assert.AreEqual(true, rsp[4].PetNames.Count == 1);
            // Validate the alphabetical sequence of Pets for 5th Owner
            petList = rsp[4].PetNames;
            Assert.AreEqual("Simba", petList[0]);
        }

        [TestMethod]
        public async Task GetCatNamesAllOwners_GenderWise()
        {
            // Moq Object
            var moqValidator = new Mock<IWebServiceUrlValidator>();
            moqValidator.Setup(x => x.IsValid(_url)).Returns(true);

            // Instantiate
            var sut = new PetsNameSorterEngine(moqValidator.Object);

            // Execute
            var result = await sut.GetSortedCatNames_AllOwnersGenderWise(_url);

            // Check if NO Error Message
            Assert.AreEqual(string.Empty, result.Item2);

            // Validate
            var rsp = result.Item1;

            // Validate the groups - Gender wise
            Assert.AreEqual(2, rsp.Count);

            // Check Gender grouped across all owner's
            Assert.AreEqual(GenderMale, rsp[0].Gender);
            Assert.AreEqual(GenderFemale, rsp[1].Gender);

            // Male Gender Pets count
            Assert.AreEqual(4, rsp[0].PetNames.Count);

            // Validate the alphabetical sequence of Pets for Male Gender
            var petList = rsp[0].PetNames;
            Assert.AreEqual("Garfield", petList[0]);
            Assert.AreEqual("Jim", petList[1]); 
            Assert.AreEqual("Max", petList[2]);
            Assert.AreEqual("Tom", petList[3]); 

            // Female Gender Pets count
            Assert.AreEqual(3, rsp[1].PetNames.Count);

            // Validate the alphabetical sequence of Pets for Female Gender
            petList = rsp[1].PetNames;
            Assert.AreEqual("Garfield", petList[0]);
            Assert.AreEqual("Simba", petList[1]); 
            Assert.AreEqual("Tabby", petList[2]); 
        }

        [TestMethod]
        public async Task InvalidUrl_CatNamesAllOwners_Genderwise()
        {
            // Moq Object
            var moqValidator = new Mock<IWebServiceUrlValidator>();
            moqValidator.Setup(x => x.IsValid(_invalidUrl)).Returns(true);

            // Instantiate
            var sut = new PetsNameSorterEngine(moqValidator.Object);

            // Execute
            var rsp = await sut.GetSortedCatNames_AllOwnersGenderWise(_invalidUrl);

            Assert.AreEqual(rsp.Item2 == "NotFound" || rsp.Item2 == "InternalServerError", true);
            Assert.AreEqual(rsp.Item1, null);
        }

        [TestMethod]
        public async Task InvalidUrl_CatNames_OwnerSpecific()
        {
            // Moq Object
            var moqValidator = new Mock<IWebServiceUrlValidator>();
            moqValidator.Setup(x => x.IsValid(_invalidUrl)).Returns(true);

            // Instantiate
            var sut = new PetsNameSorterEngine(moqValidator.Object);

            // Execute
            var rsp = await sut.GetSortedCatNames_OwnersGenderwise(_invalidUrl);

            Assert.AreEqual(rsp.Item2 == "NotFound" || rsp.Item2 == "InternalServerError", true);
            Assert.AreEqual(rsp.Item1, null);
        }

        /// <summary>
        /// De serialize failure test
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task JsonDeserializeError_CatNamesAllOwners_Genderwise()
        {
            // Moq Object
            var moqValidator = new Mock<IWebServiceUrlValidator>();
            moqValidator.Setup(x => x.IsValid(_invalidjsonUrl)).Returns(true);

            // Instantiate
            var sut = new PetsNameSorterEngine(moqValidator.Object);

            // Execute
            var rsp = await sut.GetSortedCatNames_AllOwnersGenderWise(_invalidjsonUrl);

            // This will be re produced only if the response recived from the url 
            // is NOT in correct format or malformed
            if (!string.IsNullOrEmpty(rsp.Item2))
            {
                Assert.AreEqual(true, rsp.Item2 == "Failed to De-serialize Json response" || rsp.Item2 == "Sequence contains no elements");
            }

            // The return list of Petowners will be Null or zero list
            // as it has faled De serialization
            Assert.AreEqual(true, rsp.Item1 == null || rsp.Item1.Count == 0);
        }

        /// <summary>
        /// De serialize failure test
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task JsonDeserializeError_CatNames_OwnerSpecific()
        {
            // Moq Object
            var moqValidator = new Mock<IWebServiceUrlValidator>();
            moqValidator.Setup(x => x.IsValid(_invalidjsonUrl)).Returns(true);

            // Instantiate
            var sut = new PetsNameSorterEngine(moqValidator.Object);

            // Execute
            var rsp = await sut.GetSortedCatNames_OwnersGenderwise(_invalidjsonUrl);

            // This will be re produced only if the response recived from the url 
            // is NOT in correct format or malformed
            if (!string.IsNullOrEmpty(rsp.Item2))
            {
                Assert.AreEqual(true, rsp.Item2 == "Failed to De-serialize Json response" || rsp.Item2 == "Sequence contains no elements");
            }

            // The return list of Petowners will be Null or zero list
            // as it has faled De serialization
            Assert.AreEqual(true, rsp.Item1 == null || rsp.Item1.Count == 0);
        }

        /// <summary>
        ///  Make sure to have NO internet connection to run the Test.
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetCatNames_OwnerSpecific_internet_ConnIssue()
        {
            // Moq Object
            var moqValidator = new Mock<IWebServiceUrlValidator>();
            moqValidator.Setup(x => x.IsValid(_url)).Returns(true);

            // Instantiate
            var sut = new PetsNameSorterEngine(moqValidator.Object);

            // Execute
            var result = await sut.GetSortedCatNames_OwnersGenderwise(_url);

            // Validate
            Assert.AreEqual(null, result.Item1);

            // Check the Error Message  present
            Assert.AreEqual(true, result.Item2 != string.Empty);

            // Error Message Contents
            var errArray = result.Item2.Split('|');
            if (errArray.Length == 2)
            {
                Assert.AreEqual("An error occurred while sending the request.", errArray[0]);
                Assert.AreEqual("The server name or address could not be resolved", errArray[1]);
            }
            else
                Assert.AreEqual("An error occurred while sending the request.", result.Item2);
        }
    }
}
