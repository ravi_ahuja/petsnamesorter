﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PetsNameSorter.NetCoreApp
{
    public class WebServiceUrlValidator : IWebServiceUrlValidator
    {
        public bool IsValid(string url)
        {
            // Checks for empty string
            return !string.IsNullOrEmpty(url);
        }
    }
}
