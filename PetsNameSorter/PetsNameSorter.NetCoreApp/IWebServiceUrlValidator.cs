﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PetsNameSorter.NetCoreApp
{
    public interface IWebServiceUrlValidator
    {
        bool IsValid(string url);
    }
}
