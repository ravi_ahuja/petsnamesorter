﻿using Newtonsoft.Json;
using PetsNameSorter.DataModel.Model;
using PetsNameSorter.DataModel.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PetsNameSorter.NetCoreApp
{
    public sealed class PetsNameSorterEngine
    {
        private readonly string PetType = "Cat";
        private readonly string GenderMale = "Male";
        private readonly string GenderFemale = "Female";
        private readonly IWebServiceUrlValidator _validator;

        public PetsNameSorterEngine(IWebServiceUrlValidator validator)
        {
            _validator = validator ?? throw new ArgumentNullException(nameof(validator));
        }

        /// <summary>
        /// Gets a list of cat Names sorted alphabetically
        /// under the heading gender of their respective owner.
        /// </summary>
        /// <returns>List of cat undrs the oweners gender heading</returns>
        public async Task<Tuple<List<PetsOwnerViewModel>, string>> GetSortedCatNames_OwnersGenderwise(string url)
        {
            List<PetsOwnerViewModel> petsList = null;
            var client = new HttpClient();
            
            // Error Message string
            var ErrorMsg = string.Empty;

            try
            {
                if (!_validator.IsValid(url))
                    return new Tuple<List<PetsOwnerViewModel>, string>(petsList, ErrorMsg);

                // Invoke the client
                var response = await client.GetAsync(url);

                // Check if Status Code is Successfull
                if (response.IsSuccessStatusCode)
                {
                    var jsonStr = await response.Content.ReadAsStringAsync();

                    // De-Serialize
                    var payload = JsonConvert.DeserializeObject<List<PetsOwner>>(jsonStr);
                    if (payload != null)
                    {
                        petsList = new List<PetsOwnerViewModel>();

                        petsList = (from item in payload
                               where item.Pets != null
                               select new PetsOwnerViewModel()
                               {
                                   Gender = item.Gender,
                                   PetNames = (from it in item.Pets
                                               .Where(x => x.Type == PetType)
                                               .OrderBy(y => y.Name)
                                               select it.Name)
                                              .ToList()
                               }).ToList();
                    }
                }
                // Not Success
                else
                {
                    // Return HttpStatus code when not OK to the caller.
                    ErrorMsg = response.StatusCode.ToString();
                }
            }
            catch (JsonSerializationException)
            {
                ErrorMsg = "Failed to De-serialize Json response";
            }
            catch (Exception ex)
            {
                // Handle Generic Error
                // Send more specific information(Inner Excpetion) along with outer exception errors.
                ErrorMsg = ex.InnerException != null ? ex.Message + "|" + ex.InnerException.Message: ex.Message;
            }
            finally
            {
                // Dispose the HttpClient
                client.Dispose();
            }

            return new Tuple<List<PetsOwnerViewModel>, string>(petsList, ErrorMsg);
        }

        /// <summary>
        /// Get list of Cat Names sorted alphabetically
        /// grouped Gender wise across all Owners.
        /// </summary>
        /// <returns>List of cats under distinct gender across All owners.</returns>
        public async Task<Tuple<List<PetsOwnerViewModel>, string>> GetSortedCatNames_AllOwnersGenderWise(string url)
        {
            List<PetsOwnerViewModel> petsList = null;
            var client = new HttpClient();

            var ErrorMsg = string.Empty;

            try
            {
                if (!_validator.IsValid(url))
                    return new Tuple<List<PetsOwnerViewModel>, string>(petsList, ErrorMsg);

                // Invoke the client
                var response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    var jsonStr = await response.Content.ReadAsStringAsync();

                    // De-Serialize
                    var payload = JsonConvert.DeserializeObject<List<PetsOwner>>(jsonStr);

                    if (payload != null)
                    {
                        petsList = new List<PetsOwnerViewModel>();

                        // Get All cats for all owners
                        petsList = (from item in payload
                               where item.Pets != null
                               select new PetsOwnerViewModel()
                               {
                                   Gender = item.Gender,
                                   PetNames = (from it in item.Pets
                                               .Where(x => x.Type == PetType)
                                               select it.Name)
                                              .ToList()
                               }).ToList();

                        // Group them by gender across ALL owners
                        petsList = new List<PetsOwnerViewModel>()
                        {
                            new PetsOwnerViewModel()
                            {
                                Gender = petsList.GroupBy(x => x.Gender)
                                         .Select(x => x.First().Gender).First(),
                                PetNames = petsList
                                    .Where(g => g.Gender == GenderMale)
                                    .SelectMany(x => x.PetNames)
                                    .OrderBy(x => x)
                                    .ToList()
                            },
                            new PetsOwnerViewModel()
                            {
                                Gender = petsList.GroupBy(x => x.Gender)
                                         .Select(x => x.Last().Gender).Last(),
                                PetNames = petsList
                                    .Where(g => g.Gender == GenderFemale)
                                    .SelectMany(x => x.PetNames)
                                    .OrderBy(x => x)
                                    .ToList()
                            }
                        };
                    }
                }
                // NOK Http Status Code
                else
                {
                    // Return HttpStatus code when not OK to the caller.
                    ErrorMsg = response.StatusCode.ToString();
                }
            }
            catch (JsonSerializationException)
            {
                ErrorMsg = "Failed to De-serialize Json response";
            }
            catch (Exception ex)
            {
                // Handle Generic Error
                // Send more specific information(Inner Excpetion) along with outer exception errors.
                ErrorMsg = ex.InnerException != null ? ex.Message + "|" + ex.InnerException.Message : ex.Message;
            }
            finally
            {
                // Dispose the HttpClient
                client.Dispose();
            }

            return new Tuple<List<PetsOwnerViewModel>, string>(petsList, ErrorMsg);
        }
    }
}
