﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Threading.Tasks;

namespace PetsNameSorter.NetCoreApp
{
    class Program
    {
        public static IConfiguration Configuration { get; set; }

        static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        static async Task MainAsync(string[] args)
        {
            string _url;

            // Read Config
            ReadConfiguration(out _url);

            var obj = new PetsNameSorterEngine(new WebServiceUrlValidator());

            var petsList = await obj.GetSortedCatNames_OwnersGenderwise(_url);
            Console.WriteLine("List of cat names sorted alphabetically under heading of 'Gender' of their Owner Individually");
            ProcessPetsOwnerList(petsList);

            petsList = await obj.GetSortedCatNames_AllOwnersGenderWise(_url);
            Console.WriteLine("List of cat names sorted alphabetically across all owners grouped Gender wise");
            ProcessPetsOwnerList(petsList);
        }

        private static void ReadConfiguration(out string _url)
        {
            _url = string.Empty;

            // Read Configuration once per instance via static 
            if (Configuration == null)
            {
                Configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json", false, true)
                        .Build();

                _url = Configuration["serviceUrl"];
            }
        }

        private static void ProcessPetsOwnerList(Tuple<System.Collections.Generic.List<DataModel.ViewModel.PetsOwnerViewModel>, string> resp)
        {
            foreach (var item in resp.Item1)
            {
                Console.WriteLine($"{item.Gender}");
                foreach (var it in item.PetNames)
                {
                    Console.WriteLine($"{it}");
                }
                Console.WriteLine("");
            }
        }
    }
}
