# README #
### What is this repository for? ###

Pets Name Sorter project

Description:
------------
A json file (hosted on cloud) has been set up at the url: https://api.myjson.com/bins/18cwcb

Expected Results:
-----------------
To consume the web json file and output below

- To list of all the cats in alphabetical order under heading of the gender for each owner.
Example:

Male
Angel
Molly

Female
Gizmo
Jasper

Male
Tom
Kitty

....

- To list of all the cats in alphabetical order under heading of the gender across all owners.

Example:
Male
Angel
Molly
Tigger

Female
Gizmo
Jasper

Solution:
---------
The solution has a console application written using 
the latest tech stack .net core 2(console) and unit tests for the solution.


This README would normally document whatever steps are necessary to get your application up and running.

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact